const express = require('express');
const app = express();
const port = 8080;
let data = {};

app.use(express.static('build'))

app.get('/', (req, res, next) => {
  res.sendFile(__dirname + '/build/index.html');
  next();
})

app.listen(port, (err) => {
  if (err) {
    return console.log('Error', err)
  }

  console.log(`server is listening on ${port}`)
});

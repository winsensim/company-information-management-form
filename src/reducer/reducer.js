const initialState = {
  companies: {
  },
  offices: {
  },
  meetingRooms: {
  },
  totalCompanies: 0,
  totalOffices: 0,
  totalMeetingRooms: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'CREATE_COMPANY': {
      const id = state.totalCompanies + 1;

      return {
        ...state,
        totalCompanies: id,
        companies: {
          ...state.companies,
          [id]: {
            id,
            ...action.companyData,
            totalOffices: 0,
            totalMeetingRooms: 0
          }
        }
      };
    }

    case 'CREATE_OFFICE': {
      const id = state.totalOffices + 1;

      const companies = {
        ...state.companies,
        [action.officeData.company]: {
          ...state.companies[action.officeData.company],
          totalOffices: state.companies[action.officeData.company].totalOffices + 1
        }
      };

      return {
        ...state,
        totalOffices: id,
        companies: {
          ...companies
        },
        offices: {
          ...state.offices,
          [id]: {
            id,
            ...action.officeData,
            totalMeetingRooms: 0,
          }
        }
      };
    }

    case 'CREATE_MEETING_ROOM': {
      const id = state.totalMeetingRooms + 1;
      const officeId = action.meetingRoomsData.office;
      const companyId = state.office[officeId].company;

      const companies = {
        ...state.companies,
        [companyId]: {
          ...state.companies[companyId],
          totalMeetingRooms: state.companies[companyId].totalMeetingRooms + 1
        }
      };

      const offices = {
        ...state.offices,
        [officeId]: {
          ...state.offices[officeId],
          totalMeetingRooms: state.offices[officeId].totalMeetingRooms + 1
        }
      };

      return {
        ...state,
        totalMeetingRooms: id,
        offices: {
          ...offices
        },
        meetingRooms: {
          ...state.meetingRooms,
          [id]: {
            id,
            ...action.meetingRoomsData
          }
        }
      };
    }

    case 'DELETE_COMPANY': {
      const { [action.companyId]: company, ...otherCompanies } = state.companies;

      return {
        ...state,
        companies: {
          ...otherCompanies
        },
        totalCompanies: state.totalCompanies - 1
      };
    }

    case 'DELETE_OFFICE': {
      const { [action.officeId]: office, ...otherOffices } = state.offices;
      const companyId = office.company;

      const companies = {
        ...state.companies,
        [companyId]: {
          ...state.companies[companyId],
          totalOffices: state.companies[companyId].totalOffices - 1,
          totalMeetingRooms: state.companies[companyId].totalMeetingRooms - state.offices[action.officeId].totalMeetingRooms
        }
      };

      return {
        ...state,
        totalOffices: state.totalOffices - 1,
        companies: {
          ...companies
        },
        offices: {
          ...otherOffices,
        }
      };
    }

    case 'DELETE_MEETING_ROOM': {
      const { [action.meetingRoomId]: room, ...otherMeetingRooms } = state.meetingRooms;
      const officeId = room.office;
      const companyId = state.offices[officeId].company;

      const offices = {
        ...state.offices,
        [officeId]: {
          ...state.offices[officeId],
          totalmeetingRooms: state.offices[officeId].totalMeetingRooms - 1
        }
      };

      const companies = {
        ...state.companies,
        [companyId]: {
          ...state.companies[companyId],
          totalMeetingRooms: state.companies[companyId].totalMeetingRooms - 1
        }
      };

      return {
        ...state,
        totalmeetingRooms: state.totalmeetingRooms - 1,
        companies: {
          ...companies
        },
        offices: {
          ...offices
        },
        meetingRooms: {
          ...otherMeetingRooms,
        }
      };
    }

    default:
      return state;
  }
};

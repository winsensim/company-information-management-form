import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import reducer from './reducer';

const reducers = combineReducers({
  router: routerReducer,
  store: reducer
});

export default reducers;

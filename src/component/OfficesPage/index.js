import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Link } from 'react-router-dom';
import swal from 'sweetalert';
import moment from 'moment';
import CreateMeetingRoom from '../CreateMeetingRoom'
import { deleteOffice } from '../../action';

class OfficesPage extends Component {
  constructor(props) {
    super(props);

    this.deleteOffice = this.deleteOffice.bind(this);
  }

  deleteOffice(id) {
    swal({
      text: `Delete office ${this.props.store.offices[id].name} ?`,
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this.props.deleteOffice(id);
      }
    });
  }

  render() {
    const { companyId } = this.props.match.params;
    const company = this.props.store.companies[companyId];

    if (typeof company === 'undefined') {
      const Status = ({ code, children }) => (
        <Route render={({ staticContext }) => {
          if (staticContext)
            staticContext.status = code;
          return children;
        }}/>
      );

      return (
        <Status code={404}>
          <div>
            <h1>Company not found</h1>
          </div>
        </Status>
      );
    }

    const renderOfficeWidget = (office) => (
      <div className="office" key={office.id}>
        <header className="office__name">
          {office.name}
          <button
            type="button"
            className="btn btn--delete"
            onClick={() => { this.deleteOffice(office.id) }}
          >
            &#10006;
          </button>
        </header>
        <div className="office__info">
          <div className="info-label">Location:</div>
          <div className="info-value">Lat: - {office.location_lat}</div>
          <div className="info-value">Log: - {office.location_long}</div>
          <div className="info-label">Office Start Date:</div>
          <div className="info-value">{ moment(office.start_date).format('MM/DD/YYYY') }</div>
          <div className="info-label">Number of Meeting Rooms:</div>
          <div className="info-value">{office.totalMeetingRooms}</div>
        </div>
      </div>
    );

    const { offices } = this.props.store;

    let officeWidgets = [];

    if (this.props.store.totalOffices > 0) {
      for (const office in offices) {
        if (offices.hasOwnProperty(office)) {
          const data = offices[office];

          if (data.company === companyId) {
            officeWidgets.push(renderOfficeWidget(data));
          } else {
            officeWidgets = "There is no offices created yet";
          }
        }
      }
    } else {
      officeWidgets.push("There is no offices created yet");
    }

    return (
      <div className="offices-pages">
        <div className="offices-pages__company">
          <header className="company__name">
            {company.name}
          </header>
          <div className="company__info">
            <div className="info-label">Address:</div>
            <div className="info-value">{company.address}</div>
            <div className="info-label">Revenue:</div>
            <div className="info-value">{company.revenue}</div>
            <div className="info-label">Phone No:</div>
            <div className="info-value">({company.phone_code}) {company.phone_number}</div>
            <div className="info-label">Number of Offices:</div>
            <div className="info-value">{company.totalOffices}</div>
            <div className="info-label">Number of Meeting Rooms:</div>
            <div className="info-value">{company.totalMeetingRooms}</div>
            <Link to="/" className="anchor anchor--back">Back to Overview</Link>
          </div>
        </div>
        <CreateMeetingRoom />
        <h2 className="heading">Offices</h2>
        <div className="offices__container">
          {officeWidgets}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  store: state.store
});

const mapDispatchToProps = dispatch => ({
  deleteOffice: (officeId) => dispatch(deleteOffice(officeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(OfficesPage);

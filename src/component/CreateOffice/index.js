import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notify from 'react-notification-system';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { createOffice } from '../../action';

class CreateOffice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      location_lat: "",
      location_long: "",
      start_date: moment(),
      company: "",
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    if(!this.validateForm(e)) {
      return false;
    }

    const { errors, ...noErrorsState } = this.state;

    this.props.createOffice(noErrorsState);

    this.notification.addNotification({
      level: 'success',
      position: 'tc',
      title: 'Success',
      message: `Office ${this.state.name} created`,
    });

    this.setState({
      name: "",
      location_lat: "",
      location_long: "",
      start_date: moment(),
      company: "",
      errors: {}
    });

    return true;
  }

  handleChange(e) {
    let errors = {};

    if(!e.target.checkValidity()) {
      errors = {
        [e.target.name]: e.target.validationMessage
      }
    } else {
      const { [e.target.name]: value, ...otherErrors } = this.state.errors;
      errors = otherErrors;
    }

    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...errors
      }
    });
  }

  handleChangeDate(date) {
    let errors = {};

    if(false) {
      errors = {
        start_date: 'Please fill out start date'
      }
    } else {
      const { start_date, ...otherErrors } = this.state.errors;
      errors = otherErrors;
    }

    this.setState({
      start_date: date,
      errors: {
        ...this.state.errors,
        ...errors
      }
    });
  }

  isPositiveInteger(str) {
    const n = Math.floor(Number(str));
    return String(n) === str && n >= 0;
  }

  isPositiveFloat(str) {
    const n = Number(str);
    return (String(n) === str || !isNaN(n)) && n >= 0;
  }

  validateForm(e) {
    let errors = {};

    if (this.state.name === '') {
      errors = {
        ...errors,
        name: "Please fill out name field"
      };
    }

    if (this.state.location_lat === '') {
      errors = {
        ...errors,
        location_lat: "Please fill out latitude field"
      };
    } else if (!this.isPositiveFloat(this.state.location_lat)) {
      errors = {
        ...errors,
        location_lat: "Latitude should be positive float number"
      };
    }

    if (this.state.location_long === '') {
      errors = {
        ...errors,
        location_long: "Please fill out longitude field"
      };
    } else if (!this.isPositiveFloat(this.state.location_long)) {
      errors = {
        ...errors,
        location_long: "Longitude should be positive float number"
      };
    }

    if (!this.state.start_date) {
      errors = {
        ...errors,
        start_date: "Please fill out start date field"
      };
    }

    if (this.state.company === '') {
      errors = {
        ...errors,
        company: "Please select company"
      };
    }

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  }

  render() {
    const { companies } = this.props.store;

    let companyDropdown = [];

    if (this.props.store.totalCompanies > 0) {
      for (const company in companies) {
        if (companies.hasOwnProperty(company)) {
          const data = companies[company];
          companyDropdown.push(
            <option value={data.id} key={data.id}>{data.name}</option>
          );
        }
      }
    }

    return (
      <div className="overview__form overview__form--office">
        <Notify ref={ el => { this.notification = el; }} />
        <h3 className="form-title">Create Office</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-field">
            <label htmlFor="field-office-name">Name:</label>
            <input
              id="field-office-name"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="name"
              required
            />
            {this.state.errors.name ? (
              <div className="error-message">{this.state.errors.name}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label id="office-location-label" htmlFor="field-office-location-lat">Location:</label>
            <input
              id="field-office-location-lat"
              name="location_lat"
              value={this.state.location_lat}
              onChange={this.handleChange}
              className="field field--lat"
              type="number"
              min="0"
              step="0.000000001"
              placeholder="latitude"
              aria-labelledby="office-location-label"
              required
            />
            <input
              id="field-office-location-long"
              name="location_long"
              value={this.state.location_long}
              onChange={this.handleChange}
              className="field field--long"
              type="number"
              min="0"
              step="0.000000001"
              placeholder="longitude"
              aria-labelledby="office-location-label"
              required
            />
            {this.state.errors.location_lat ? (
              <div className="error-message">{this.state.errors.location_lat}</div>
            ) : null}
            {this.state.errors.location_long ? (
              <div className="error-message">{this.state.errors.location_long}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-office-start-date">Office Start Date:</label>
            <DatePicker
              id="field-office-start-date"
              name="start_date"
              selected={this.state.start_date}
              onChange={this.handleChangeDate}
              className="field"
              placeholderText="date"
              required
            />
            {this.state.errors.start_date ? (
              <div className="error-message">{this.state.errors.start_date}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-company">Company:</label>
            <select
              id="field-company"
              name="company"
              value={this.state.company}
              onChange={this.handleChange}
              className="field"
              type="text"
              required
            >
              <option value="" disabled>select company</option>
              {companyDropdown}
            </select>
            {this.state.errors.company ? (
              <div className="error-message">{this.state.errors.company}</div>
            ) : null}
          </div>
          <button type="submit" className="btn btn--submit">Create</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  store: state.store
});

const mapDispatchToProps = dispatch => ({
  createOffice: (officeData) => dispatch(createOffice(officeData))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateOffice);

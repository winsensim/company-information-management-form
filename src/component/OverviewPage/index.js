import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import CreateCompany from '../CreateCompany';
import CreateOffice from '../CreateOffice';
import { deleteCompany } from '../../action';

class OverviewPage extends Component {
  constructor(props) {
    super(props);

    this.deleteCompany = this.deleteCompany.bind(this);
  }

  deleteCompany(id) {
    swal({
      text: `Delete company ${this.props.store.companies[id].name} ?`,
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this.props.deleteCompany(id);
      }
    });
  }

  render() {
    const renderCompanyWidget = (company) => (
      <div className="company" key={company.id}>
        <header className="company__name">
          <Link to={`/${company.id}`}>{company.name}</Link>
          <button
            type="button"
            className="btn btn--delete"
            onClick={() => this.deleteCompany(company.id)}
          >
            &#10006;
          </button>
        </header>
        <Link to={`/${company.id}`}>
          <div className="company__info">
            <div className="info-label">Address:</div>
            <div className="info-value">{company.address}</div>
            <div className="info-label">Revenue:</div>
            <div className="info-value">{company.revenue}</div>
            <div className="info-label">Phone No:</div>
            <div className="info-value">({company.phone_code}) {company.phone_number}</div>
            <div className="info-label">Number of Offices:</div>
            <div className="info-value">{company.totalOffices}</div>
            <div className="info-label">Number of Meeting Rooms:</div>
            <div className="info-value">{company.totalMeetingRooms}</div>
          </div>
        </Link>
      </div>
    );

    const { companies } = this.props.store;

    let companyWidgets = [];

    if (this.props.store.totalCompanies > 0) {
      for (const company in companies) {
        if (companies.hasOwnProperty(company)) {
          const data = companies[company];
          companyWidgets.push(renderCompanyWidget(data));
        }
      }
    } else {
      companyWidgets.push("There is no companies created yet");
    }

    return (
      <div className="overview">
        <div className="overview__create">
          <CreateCompany />
          <CreateOffice />
        </div>
        <div className="overview__container">
          <h2 className="heading">Companies</h2>
          <div className="overview__companies">
            {companyWidgets}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  store: state.store
});

const mapDispatchToProps = dispatch => ({
  deleteCompany: companyId => dispatch(deleteCompany(companyId))
});

export default connect(mapStateToProps, mapDispatchToProps)(OverviewPage);

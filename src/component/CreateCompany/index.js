import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notify from 'react-notification-system';
import { createCompany } from '../../action';

class CreateCompany extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      address: "",
      revenue: "",
      phone_code: "",
      phone_number: "",
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    if(!this.validateForm(e)) {
      return false;
    }

    const { errors, ...noErrorsState } = this.state;

    this.props.createCompany(noErrorsState);

    this.notification.addNotification({
      level: 'success',
      position: 'tc',
      title: 'Success',
      message: `Company ${this.state.name} created`,
    });

    this.setState({
      name: "",
      address: "",
      revenue: "",
      phone_code: "",
      phone_number: "",
      errors: {}
    });

    return true;
  }

  handleChange(e) {
    let errors = {};

    if(!e.target.checkValidity()) {
      errors = {
        [e.target.name]: e.target.validationMessage
      }
    } else {
      const { [e.target.name]: value, ...otherErrors } = this.state.errors;
      errors = otherErrors;
    }

    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...errors
      }
    });
  }

  isPositiveInteger(str) {
    const n = Math.floor(Number(str));
    return String(n) === str && n >= 0;
  }

  isPositiveFloat(str) {
    const n = Number(str);
    return (String(n) === str || !isNaN(n)) && n >= 0;
  }

  validateForm(e) {
    let errors = {};

    if (this.state.name === '') {
      errors = {
        ...errors,
        name: "Please fill out name field"
      };
    }

    if (this.state.address === '') {
      errors = {
        ...errors,
        address: "Please fill out address field"
      };
    }

    if (this.state.revenue === '') {
      errors = {
        ...errors,
        revenue: "Please fill out revenue field"
      };
    } else if (!this.isPositiveFloat(this.state.revenue)) {
      errors = {
        ...errors,
        revenue: "Revenue should be positive float number"
      };
    }

    if (this.state.phone_code === '') {
      errors = {
        ...errors,
        phone_code: "Please fill out phone code field"
      };
    } else if (!this.isPositiveInteger(this.state.phone_code)) {
      errors = {
        ...errors,
        phone_code: "Phone code should be positive integer"
      };
    }

    if (this.state.phone_number === '') {
      errors = {
        ...errors,
        phone_number: "Please fill out phone number field"
      };
    } else if (!this.isPositiveInteger(this.state.phone_number)) {
      errors = {
        ...errors,
        phone_number: "Phone number should be positive integer"
      };
    }

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  }

  render() {
    return (
      <div className="overview__form overview__form--company">
        <Notify ref={ el => { this.notification = el; }} />
        <h3 className="form-title">Create Company</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-field">
            <label htmlFor="field-company-name">Name:</label>
            <input
              id="field-company-name"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="name"
              required
            />
            {this.state.errors.name ? (
              <div className="error-message">{this.state.errors.name}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-company-address">Address:</label>
            <input
              id="field-company-address"
              name="address"
              value={this.state.address}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="address"
              required
            />
            {this.state.errors.address ? (
              <div className="error-message">{this.state.errors.address}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-company-revenue">Revenue:</label>
            <input
              id="field-company-revenue"
              name="revenue"
              value={this.state.revenue}
              onChange={this.handleChange}
              className="field"
              type="number"
              min="0"
              step="0.0001"
              placeholder="revenue"
              required
            />
            {this.state.errors.revenue ? (
              <div className="error-message">{this.state.errors.revenue}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label id="company-phone-label" htmlFor="field-company-phone-code">Phone No:</label>
            <input
              id="field-company-phone-code"
              name="phone_code"
              value={this.state.phone_code}
              onChange={this.handleChange}
              className="field field--code"
              type="number"
              min="0"
              step="1"
              placeholder="code"
              aria-labelledby="company-phone-label"
              required
            />
            <input
            id="field-company-phone-number"
              name="phone_number"
              value={this.state.phone_number}
              onChange={this.handleChange}
              className="field field--phone"
              type="number"
              min="0"
              step="1"
              placeholder="number"
              aria-labelledby="company-phone-label"
              required
            />
            {this.state.errors.phone_code ? (
              <div className="error-message">{this.state.errors.phone_code}</div>
            ) : null}
            {this.state.errors.phone_number ? (
              <div className="error-message">{this.state.errors.phone_number}</div>
            ) : null}
          </div>
          <button type="submit" className="btn btn--submit">Create</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  store: state.store
});

const mapDispatchToProps = dispatch => ({
  createCompany: companyData => dispatch(createCompany(companyData))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateCompany);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notify from 'react-notification-system';
import { createMeetingRoom } from '../../action';

class CreateMeetingRoom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      location: "",
      floor: "",
      size: "",
      number_of_seats: "",
      office: "",
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    if(!this.validateForm(e)) {
      return false;
    }

    const { errors, ...noErrorsState } = this.state;

    this.props.createMeetingRoom(noErrorsState);

    this.notification.addNotification({
      level: 'success',
      position: 'tc',
      title: 'Success',
      message: `Meeting room ${this.state.name} created`,
    });

    this.setState({
      name: "",
      location: "",
      floor: "",
      size: "",
      number_of_seats: "",
      office: "",
      errors: {}
    });

    return true;
  }

  handleChange(e) {
    let errors = {};

    if(!e.target.checkValidity()) {
      errors = {
        [e.target.name]: e.target.validationMessage
      }
    } else {
      const { [e.target.name]: value, ...otherErrors } = this.state.errors;
      errors = otherErrors;
    }

    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...errors
      }
    });
  }

  isPositiveInteger(str) {
    const n = Math.floor(Number(str));
    return String(n) === str && n >= 0;
  }


  validateForm(e) {
    let errors = {};

    if (this.state.name === '') {
      errors = {
        ...errors,
        name: "Please fill out name field"
      };
    }

    if (this.state.location === '') {
      errors = {
        ...errors,
        location: "Please fill out location field"
      };
    }

    if (this.state.floor === '') {
      errors = {
        ...errors,
        floor: "Please fill out floor field"
      };
    }

    if (this.state.size === '') {
      errors = {
        ...errors,
        size: "Please fill out size field"
      };
    }

    if (this.state.number_of_seats === '') {
      errors = {
        ...errors,
        number_of_seats: "Please fill out number of seats field"
      };
    } else if (!this.isPositiveInteger(this.state.number_of_seats)) {
      errors = {
        ...errors,
        location_lat: "Number of seats should be positive integer"
      };
    }

    if (this.state.office === '') {
      errors = {
        ...errors,
        office: "Please select office"
      };
    }

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  }

  render() {
    const { offices } = this.props.store;

    let officeDropdown = [];

    if (this.props.store.totalOffices > 0) {
      for (const office in offices) {
        if (offices.hasOwnProperty(office)) {
          const data = offices[office];
          officeDropdown.push(
            <option value={data.id} key={data.id}>{data.name}</option>
          );
        }
      }
    }

    return (
      <div className="overview__form overview__form--office">
        <Notify ref={ el => { this.notification = el; }} />
        <h3 className="form-title">Create Meeting Room</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-field">
            <label htmlFor="field-mr-name">Name:</label>
            <input
              id="field-mr-name"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="name"
              required
            />
            {this.state.errors.name ? (
              <div className="error-message">{this.state.errors.name}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-mr-location">Location:</label>
            <input
              id="field-mr-location"
              name="location"
              value={this.state.location}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="location"
              required
            />
            {this.state.errors.location ? (
              <div className="error-message">{this.state.errors.location}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-mr-floor">Floor:</label>
            <input
              id="field-mr-floor"
              name="floor"
              value={this.state.floor}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="floor"
              required
            />
            {this.state.errors.floor ? (
              <div className="error-message">{this.state.errors.floor}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-mr-size">Size:</label>
            <input
              id="field-mr-size"
              name="size"
              value={this.state.size}
              onChange={this.handleChange}
              className="field"
              type="text"
              placeholder="size"
              required
            />
            {this.state.errors.size ? (
              <div className="error-message">{this.state.errors.size}</div>
            ) : null}
          </div>
          <div className="form-field">
            <label htmlFor="field-mr-seats">Number Of Seats:</label>
            <input
              id="field-mr-seats"
              name="number_of_seats"
              value={this.state.number_of_seats}
              onChange={this.handleChange}
              className="field"
              type="number"
              min="0"
              step="1"
              placeholder="Number Of Seats"
              required
            />
            {this.state.errors.number_of_seats ? (
              <div className="error-message">{this.state.errors.number_of_seats}</div>
            ) : null}
          </div>

          <div className="form-field">
            <label htmlFor="field-office">Office:</label>
            <select
              id="field-office"
              name="office"
              value={this.state.office}
              onChange={this.handleChange}
              className="field"
              type="text"
              required
            >
              <option value="" disabled>select office</option>
              {officeDropdown}
            </select>
            {this.state.errors.office ? (
              <div className="error-message">{this.state.errors.office}</div>
            ) : null}
          </div>
          <button type="submit" className="btn btn--submit">Create</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  store: state.store
});

const mapDispatchToProps = dispatch => ({
  createMeetingRoom: (meetingRoomData) => dispatch(createMeetingRoom(meetingRoomData))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateMeetingRoom);

export const createCompany = companyData => dispatch => {
  dispatch({
    type: 'CREATE_COMPANY',
    companyData
  });
};

export const deleteCompany = companyId => dispatch => {
  dispatch({
    type: 'DELETE_COMPANY',
    companyId
  });
};

export const createOffice = officeData => dispatch => {
  dispatch({
    type: 'CREATE_OFFICE',
    officeData
  });
};

export const deleteOffice = officeId => dispatch => {
  dispatch({
    type: 'DELETE_OFFICE',
    officeId
  });
};

export const createMeetingRoom = meetingRoomsData => dispatch => {
  dispatch({
    type: 'CREATE_MEETING_ROOM',
    meetingRoomsData
  });
};

export const deleteMeetingRooms = meetingRoomId => dispatch => {
  dispatch({
    type: 'DELETE__MEETING_ROOM',
    meetingRoomId
  });
};